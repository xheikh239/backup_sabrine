-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: SABRINE
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `SABRINE`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `SABRINE` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `SABRINE`;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('Sajeeb','Sajeeb'),('admin','admin'),('Shafayat','Shafayat'),('Mansur','Mansur');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `dName` varchar(20) NOT NULL,
  `pName` varchar(15) NOT NULL,
  `room` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES ('Sajeeb','Shafayat Jamil',987),('Sifat','Sajeeb Shahriar',806);
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctor` (
  `count` int NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `id` varchar(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `age` int NOT NULL,
  `gender` varchar(8) NOT NULL,
  `blood` varchar(4) NOT NULL,
  `dept` varchar(20) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL,
  `address` varchar(20) NOT NULL,
  `room` int NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`count`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'02-04-2018','sjb0HMSd1','Sifat Islam',20,'Male','A-','Medecine','+8801757009622','sajeeb3g@gmail.com','Single','Faridpur',806,'Sifat','Sifat'),(2,'31-01-2018','sjb0HMSd2','Mansorol Islam',32,'Male','A+','Dental','+8801665546776','mansu@gmail.com','Married','Gazipur',202,'Mansu','Mansu'),(3,'12-12-2016','sjb0HMSd3','Nafees Khan',34,'Male','AB-','Neurology','+8801734253425','nafees@gmail.com','Divorced','Patuakhali',809,'Nafees','Nafees'),(4,'21-0802015','sjb0HMSd4','Shafayat Jamil',43,'Male','B+','Nutrition','+8801754323454','shafayat@gmail.com','Single','Bogra',44,'Shafayat','Shafayat'),(5,'09-08-2017','sjb0HMSd4','Nusrat Jaman',23,'Female','O-','Gynaecology','+8801765432187','nusrat@gmail.com','Single','Rajshahi',909,'Nusrat','Nusrat'),(6,'06-03-2016','sjb0HMSd6','Fahad Ali',37,'Male','B-','Cardiology','+8801754321678','fahad@gmail.com','Married','Kustia',545,'Fahad','Fahad'),(7,'05-03-2015','sjb0HMSd7','Sanjida Sultana',28,'Female','O+','Haematology','+8801765432134','sanjida@gmail.com','Married','Natore',23,'Sanjida','Sanjida'),(8,'21-12-2013','sjb0HMSd8','Fazle Rabby',44,'Male','B-','Microbiology','+8801745321376','rabby@gmail.com','Married','Brahmanbaria',777,'Rabby','Rabby'),(9,'27-09-2009','sjb0HMSd9','Marina Naznin',22,'Female','O+','Gynaecology','+8801756789006','marina@gmail.com','Single','Tangail',555,'Marina','Marina'),(12,'12-23-2015','sjb0HMSd9','Sajeeb Shahriar',21,'Male','O+','Microbiology','+8801756432156','sajeeb@gmail.com','Single','Rajshahi',987,'Sajeeb','Sajeeb');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `count` int NOT NULL AUTO_INCREMENT,
  `date` varchar(10) NOT NULL,
  `id` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `age` int NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  `disease` varchar(20) NOT NULL,
  `room` int NOT NULL,
  PRIMARY KEY (`count`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (2,'08-03-2018','sjb0HMS2','Sajeeb Shahriar',20,'Male','Naldanga, Natore','+8801757009622','Single','Feaver',504),(3,'09-06-2018','sjb0HMS3','Joy Bhowmik',21,'Male','Vola','+8801876543237','Single','Dengue',102),(4,'03-05-2017','sjb0HMS4','Fahad Mondol',78,'Male','Kustia','+8801889765426','Married','Jondish',653),(5,'09-03-2016','sjb0HMS5','Fazle Rabby',24,'Male','Brahmanbariya','+8801765432124','Divorced','Malaria',806),(6,'22-12-2017','sjb0HMS6','Shafayat Jamil',27,'Male','Bogra','+8801722456657','Married','Tyfoyed',122),(8,'22-12-2017','sjb0HMS7','Nazmus Sakib',22,'Male','Chadpur','+8801754678907','Single','Feaver',22);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptionist`
--

DROP TABLE IF EXISTS `receptionist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `receptionist` (
  `count` int NOT NULL AUTO_INCREMENT,
  `joining` varchar(15) NOT NULL,
  `id` varchar(15) NOT NULL,
  `name` varchar(30) NOT NULL,
  `age` int NOT NULL,
  `gender` varchar(10) NOT NULL,
  `blood` varchar(4) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(17) NOT NULL,
  `address` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`count`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptionist`
--

LOCK TABLES `receptionist` WRITE;
/*!40000 ALTER TABLE `receptionist` DISABLE KEYS */;
INSERT INTO `receptionist` VALUES (3,'23-04-2013','sjb0HMSd3','Sajeeb Shahriar',21,'Male','O+','sajeeb3g@gmail.com','+8801757009622','Naldanga,Natore','Single','Sajeeb','Sajeeb'),(4,'23-06-2018','sjb0HMSr3','Mansorol Islam',24,'Male','A-','mansu@gmail.com','+8801654321234','Gazipur','Married','Mansu','Mansu'),(6,'12-06-2018','sjb0HMSr4','Nafees Khan',24,'Male','AB-','nafees@gmail.com','+8801767575658','Patuakhali','Single','Nafees','Nafees'),(9,'07-12-2018','sjb0HMSr7','Sifat Khan',34,'Male','O+','sifat@gmail.com','+8801766565676','Faridpur','Married','Sifat','Sifat'),(10,'23-12-2018','sjb0HMSr9','Shafayat Jamil',44,'Male','AB-','shafayat@gmail.com','+88017654323','Bogra','Single','Shafayat','Shafayat');
/*!40000 ALTER TABLE `receptionist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test` (
  `a` varchar(11) NOT NULL,
  `b` varchar(11) NOT NULL,
  `c` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES ('5','6','6'),('1','2','4'),('gfhf','fv','hf');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-26 18:00:00
